package mwork.stud.po.library.modules.loans.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import mwork.stud.po.library.models.Book;
import mwork.stud.po.library.models.Customer;
import mwork.stud.po.library.models.Loan;
import mwork.stud.po.library.services.ServiceLayer;

import java.util.List;
import java.util.Optional;

/**
 * Main Controller of Loan module
 */
public class LoanDetailsController {

    public TableView booksTable;
    public TableColumn bookAuthorColumn;
    public TableColumn bookTitleColumn;
    public TextField booksAuthorFilter;
    public TextField booksTitleFilter;

    public TableView customersTable;
    public TableColumn customerSurnameColumn;
    public TableColumn customerForenameColumn;
    public TableColumn customerEmailColumn;
    public TextField customerSurnameFilter;
    public TextField customerForenameFilter;
    public TextField customerEmailFilter;

    public TextField bookAuthorText;
    public TextField bookTitleText;
    public TextField customerNameText;

    public Button returnButton;
    public Button borrowButton;

    public TitledPane customerPanel;
    public TitledPane bookPanel;
    public TextField issueDateText;
    public TextField returnDateText;

    private ServiceLayer service;

    private Loan loan;

    private ObservableList<Book> booksList = FXCollections.observableArrayList();
    private ObservableList<Customer> customersList = FXCollections.observableArrayList();

    public LoanDetailsController(){
        this.service = new ServiceLayer();
    }

    /**
     * Filling view on init
     * @param loanId Unique of Loan - if less than 0, new Loan will be created
     * @param bookId Unique of Book
     * @param customerId Unique of Customer
     */
    public void fillView(int loanId, int bookId, int customerId){

        this.fillBooksTable();
        this.fillCustomersTable();

        if (loanId > 0) {
            this.loan = service.GetLoan(loanId);

            if (this.loan.getIssueDate() != null) {
                issueDateText.setText(this.loan.getIssueDate().toString());
            }

            if (this.loan.getReturnDate() != null) {
                returnDateText.setText(this.loan.getReturnDate().toString());
            }
        } else {
            loan = new Loan();
            bookAuthorText.setText("");
            bookTitleText.setText("");
            customerNameText.setText("");
        }

        if (this.loan.getBook() == null && bookId > 0){
            this.loan.setBook(service.GetBook(bookId));
        }

        if (this.loan.getCustomer() == null && customerId > 0){
            this.loan.setCustomer(service.GetCustomer(customerId));
        }

        if (this.loan.getBook() != null){
            bookPanel.setExpanded(false);
            bookPanel.setCollapsible(false);
            bookAuthorText.setText(this.loan.getBookAuthor());
            bookTitleText.setText(this.loan.getBookTitle());
        }

        if (this.loan.getCustomer() != null){
            customerPanel.setExpanded(false);
            customerPanel.setCollapsible(false);
            customerNameText.setText(this.loan.getCustomerName());
        }

        setButtonsState();
    }

    private void setButtonsState(){
        if (this.loan.getIssueDate() != null || this.loan.getBook() == null || this.loan.getCustomer() == null){
            borrowButton.setDisable(true);
        } else {
            borrowButton.setDisable(false);
        }

        if (this.loan.getReturnDate() != null || this.loan.getIssueDate() == null){
            returnButton.setDisable(true);
        } else {
            returnButton.setDisable(false);
        }
    }

    public void filterBooksTable(KeyEvent keyEvent) {
        FilteredList<Book> filteredList = new FilteredList<>(booksList, p -> true);

        filteredList.setPredicate(item -> {

            if (booksAuthorFilter.getLength() > 0 && !item.getAuthor().toLowerCase().contains(booksAuthorFilter.getText().toLowerCase())){
                return false;
            }

            if (booksTitleFilter.getLength() > 0 && !item.getTitle().toLowerCase().contains(booksTitleFilter.getText().toLowerCase())){
                return false;
            }

            return true;

        });

        SortedList<Book> sortedData = new SortedList<>(filteredList);

        sortedData.comparatorProperty().bind(booksTable.comparatorProperty());

        booksTable.setItems(sortedData);
    }

    public void filterCustomerTable(KeyEvent keyEvent) {
        FilteredList<Customer> filteredList = new FilteredList<>(customersList, p -> true);

        filteredList.setPredicate(item -> {

            if (customerSurnameFilter.getLength() > 0 && !item.getLastname().toLowerCase().contains(customerSurnameFilter.getText().toLowerCase())){
                return false;
            }

            if (customerForenameFilter.getLength() > 0 && !item.getForename().toLowerCase().contains(customerForenameFilter.getText().toLowerCase())){
                return false;
            }

            if (customerEmailFilter.getLength() > 0 && !item.getEmail().toLowerCase().contains(customerEmailFilter.getText().toLowerCase())){
                return false;
            }

            return true;

        });

        SortedList<Customer> sortedData = new SortedList<>(filteredList);

        sortedData.comparatorProperty().bind(customersTable.comparatorProperty());

        customersTable.setItems(sortedData);
    }

    private void fillBooksTable(){
        bookAuthorColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("author"));
        bookTitleColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("title"));

        List<Book> books = service.GetBooksWithoutLoans();
        booksList = FXCollections.observableArrayList();
        for (Book item : books) {
            booksList.add(item);
        }
        this.filterBooksTable(null);
    }

    private void fillCustomersTable(){
        customerSurnameColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("lastname"));
        customerForenameColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("forename"));
        customerEmailColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("email"));

        List<Customer> customers = service.GetCustomersWithoutLoans();
        customersList = FXCollections.observableArrayList();
        for (Customer item : customers) {
            customersList.add(item);
        }
        this.filterCustomerTable(null);
    }

    public void returnButton(ActionEvent actionEvent) {
        try {
            Alert alertConfirmation = new Alert(Alert.AlertType.CONFIRMATION, "Czy na pewno chcesz zwrócić książkę?");
            Optional<ButtonType> result = alertConfirmation.showAndWait();

            if (result.get() == ButtonType.OK){
                service.ReturnBook(this.loan);
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Książka została zwrócona");
                alert.show();
                this.fillView(this.loan.getId(), this.loan.getBook().getId(), this.loan.getCustomer().getId());
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.show();
            e.printStackTrace();
        }
    }

    public void borrowButton(ActionEvent actionEvent) {
        try {
            Alert alertConfirmation = new Alert(Alert.AlertType.CONFIRMATION, "Czy na pewno chcesz wypożyczyć książkę?");
            Optional<ButtonType> result = alertConfirmation.showAndWait();

            if (result.get() == ButtonType.OK){
                service.BorrowBook(this.loan);
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Książka została wypożyczona");
                alert.show();
                this.fillView(this.loan.getId(), this.loan.getBook().getId(), this.loan.getCustomer().getId());
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.show();
            e.printStackTrace();
        }
    }

    public void selectCustomer(MouseEvent mouseEvent) {
        TableView<Customer> table = (TableView<Customer>)mouseEvent.getSource();
        TableView.TableViewSelectionModel<Customer> row = table.getSelectionModel();
        if (!row.isEmpty()){
            this.loan.setCustomer(row.getSelectedItem());
            this.customerNameText.setText(this.loan.getCustomerName());
        }
        setButtonsState();
    }

    public void selectBook(MouseEvent mouseEvent) {
        TableView<Book> table = (TableView<Book>)mouseEvent.getSource();
        TableView.TableViewSelectionModel<Book> row = table.getSelectionModel();
        if (!row.isEmpty()){
            this.loan.setBook(row.getSelectedItem());
            this.bookTitleText.setText(this.loan.getBookTitle());
            this.bookAuthorText.setText(this.loan.getBookAuthor());
        }
        setButtonsState();
    }
}
