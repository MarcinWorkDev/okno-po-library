package mwork.stud.po.library.modules.main.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import mwork.stud.po.library.models.Book;
import mwork.stud.po.library.models.Customer;
import mwork.stud.po.library.modules.books.controllers.BookDetailsController;
import mwork.stud.po.library.modules.customers.controllers.CustomerDetailsController;
import mwork.stud.po.library.modules.loans.controllers.LoanDetailsController;
import mwork.stud.po.library.services.ServiceLayer;

import java.util.List;

/**
 * Main Controller of Application
 */
public class MainController {
    public TableView<Customer> customersTable;
    public TextField customerIdFilter;
    public TextField customerForenameFilter;
    public TextField customerSurnameFilter;
    public TextField customerEmailFilter;
    public TableColumn customerIdColumn;
    public TableColumn customerSurnameColumn;
    public TableColumn customerForenameColumn;
    public TableColumn customerEmailColumn;
    public Button addCustomerButton;

    public TableView<Book> booksTable;
    public TextField booksAuthorFilter;
    public TextField booksTitleFilter;
    public TableColumn bookAuthorColumn;
    public TableColumn bookTitleColumn;
    public Button addBookButton;
    public Button refreshDataButton;
    public Button closeAppButton;
    public Button borrowBook;

    private ServiceLayer service;

    private ObservableList<Book> booksList = FXCollections.observableArrayList();
    private ObservableList<Customer> customersList = FXCollections.observableArrayList();

    public MainController(){
        // Initialize service
        service = new ServiceLayer();
    }

    private void fillBooksTable(){
        bookAuthorColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("author"));
        bookTitleColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("title"));

        List<Book> books = service.GetBooksWithoutLoans();
        booksList = FXCollections.observableArrayList();
        for (Book item : books) {
            booksList.add(item);
        }
        this.filterBooksTable(null);
    }

    private void fillCustomersTable(){
        customerIdColumn.setCellValueFactory(new PropertyValueFactory<Customer, Integer>("id"));
        customerSurnameColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("lastname"));
        customerForenameColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("forename"));
        customerEmailColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("email"));

        List<Customer> customers = service.GetCustomersWithoutLoans();
        customersList = FXCollections.observableArrayList();
        for (Customer item : customers) {
            customersList.add(item);
        }
        this.filterCustomerTable(null);
    }

    public void onBooksTabChange(Event event) {
        Tab tab = (Tab)event.getSource();
        if (tab.isSelected()){
            this.fillBooksTable();
        }
    }

    public void onCustomersTabChange(Event event) {
        Tab tab = (Tab)event.getSource();
        if (tab.isSelected()){
            this.fillCustomersTable();
        }
    }

    public void filterCustomerTable(KeyEvent keyEvent) {
        FilteredList<Customer> filteredList = new FilteredList<>(customersList, p -> true);

        filteredList.setPredicate(item -> {

            if (customerIdFilter.getLength() > 0 && String.valueOf(item.getId()).toLowerCase().contains(customerIdFilter.getText().toLowerCase())){
                return false;
            }

            if (customerSurnameFilter.getLength() > 0 && !item.getLastname().toLowerCase().contains(customerSurnameFilter.getText().toLowerCase())){
                return false;
            }

            if (customerForenameFilter.getLength() > 0 && !item.getForename().toLowerCase().contains(customerForenameFilter.getText().toLowerCase())){
                return false;
            }

            if (customerEmailFilter.getLength() > 0 && !item.getEmail().toLowerCase().contains(customerEmailFilter.getText().toLowerCase())){
                return false;
            }

            return true;

        });

        SortedList<Customer> sortedData = new SortedList<>(filteredList);

        sortedData.comparatorProperty().bind(customersTable.comparatorProperty());

        customersTable.setItems(sortedData);
    }

    public void filterBooksTable(KeyEvent keyEvent) {
        FilteredList<Book> filteredList = new FilteredList<>(booksList, p -> true);

        filteredList.setPredicate(item -> {

            if (booksAuthorFilter.getLength() > 0 && !item.getAuthor().toLowerCase().contains(booksAuthorFilter.getText().toLowerCase())){
                return false;
            }

            if (booksTitleFilter.getLength() > 0 && !item.getTitle().toLowerCase().contains(booksTitleFilter.getText().toLowerCase())){
                return false;
            }

            return true;

        });

        SortedList<Book> sortedData = new SortedList<>(filteredList);

        sortedData.comparatorProperty().bind(booksTable.comparatorProperty());

        booksTable.setItems(sortedData);
    }

    public void showBookDetailsView(MouseEvent mouseEvent) {
        TableView<Book> table = (TableView<Book>)mouseEvent.getSource();
        TableView.TableViewSelectionModel<Book> row = table.getSelectionModel();
        if (mouseEvent.getClickCount() > 1 && !row.isEmpty()){
            try {
                Book book = row.getSelectedItem();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../../books/views/bookDetails.fxml"));
                Parent root = loader.load();
                BookDetailsController controller = loader.getController();
                controller.fillView(book.getId());
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.setTitle("Szczegóły książki");
                stage.setResizable(false);
                stage.show();
            } catch(Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
                alert.show();
                e.printStackTrace();
            }
        }
    }

    public void showCustomerDetailsView(MouseEvent mouseEvent) {
        TableView<Customer> table = (TableView<Customer>)mouseEvent.getSource();
        TableView.TableViewSelectionModel<Customer> row = table.getSelectionModel();
        if (mouseEvent.getClickCount() > 1 && !row.isEmpty()){
            try {
                Customer customer = row.getSelectedItem();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../../customers/views/customerDetails.fxml"));
                Parent root = loader.load();
                CustomerDetailsController controller = loader.getController();
                controller.fillView(customer.getId());
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.setTitle("Szczegóły czytelnika");
                stage.setResizable(false);
                stage.show();
            } catch(Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
                alert.show();
                e.printStackTrace();
            }
        }
    }

    public void addBookButton(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../books/views/bookDetails.fxml"));
            Parent root = loader.load();
            BookDetailsController controller = loader.getController();
            controller.fillView(-1);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Szczegóły książki");
            stage.setResizable(false);
            stage.show();
        } catch(Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.show();
            e.printStackTrace();
        }
    }

    public void addCustomerButton(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../customers/views/customerDetails.fxml"));
            Parent root = loader.load();
            CustomerDetailsController controller = loader.getController();
            controller.fillView(-1);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Szczegóły czytelnika");
            stage.setResizable(false);
            stage.show();
        } catch(Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.show();
            e.printStackTrace();
        }
    }

    public void refreshData(ActionEvent actionEvent) {
        fillCustomersTable();
        fillBooksTable();
    }

    public void closeApp(ActionEvent actionEvent) {
        Stage stage = (Stage)((Button)actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    public void showLoanDetails(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../loans/views/loanDetails.fxml"));
            Parent root = loader.load();
            LoanDetailsController controller = loader.getController();
            controller.fillView(-1, -1, -1);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Szczegóły wypożyczenia książki");
            stage.setResizable(false);
            stage.show();
        } catch(Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.show();
            e.printStackTrace();
        }
    }
}
