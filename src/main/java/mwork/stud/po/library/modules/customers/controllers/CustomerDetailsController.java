package mwork.stud.po.library.modules.customers.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import mwork.stud.po.library.models.Customer;
import mwork.stud.po.library.models.Loan;
import mwork.stud.po.library.modules.loans.controllers.LoanDetailsController;
import mwork.stud.po.library.services.ServiceLayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Main Controller of Customer module
 */
public class CustomerDetailsController {
    public TextField forenameText;
    public TextField lastnameText;
    public TextField emailText;

    public TableView loansTable;
    public TableColumn issueDateColumn;
    public TableColumn returnDateColumn;
    public TableColumn bookColumn;

    public Text customerNameStaticText;
    public Text emailStaticText;
    public Button borrowBookButton;

    private ServiceLayer service;

    private Customer customer;
    private ObservableList<Loan> loansList = FXCollections.observableArrayList();

    public CustomerDetailsController(){
        this.service = new ServiceLayer();
    }

    /**
     * Filling view on init
     * @param customerId Unique of Customer
     */
    public void fillView(int customerId){

        if (customerId > 0) {
            this.customer = service.GetCustomer(customerId);

            customerNameStaticText.setText(customer.getLastname() + " " + customer.getForename());
            emailStaticText.setText(customer.getEmail());

            lastnameText.setText(customer.getLastname());
            forenameText.setText(customer.getForename());
            emailText.setText(customer.getEmail());

            bookColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("bookTitle"));
            issueDateColumn.setCellValueFactory(new PropertyValueFactory<Loan, Date>("issueDate"));
            returnDateColumn.setCellValueFactory(new PropertyValueFactory<Loan, Date>("returnDate"));

            List<Loan> loans = customer.getLoans();
            loansList = FXCollections.observableArrayList();
            for (Loan item : loans) {
                loansList.add(item);
            }
            loansTable.setItems(loansList);

        } else {
            customer = new Customer();
            customerNameStaticText.setText("");
            emailStaticText.setText("");
        }
    }

    public void saveChangesButton(ActionEvent actionEvent) {

        String lastname = lastnameText.getText();
        String forename = forenameText.getText();
        String email = emailText.getText();

        List<String> validationErrors = new ArrayList<>();
        if (lastname.trim().length() == 0){
            validationErrors.add("Nie podano nazwiska");
        }
        if (forename.trim().length() == 0){
            validationErrors.add("Nie podano imienia");
        }

        if (validationErrors.size() > 0){
            String errorMessage = "Wprowadzone dane nie są prawidłowe:";
            for (String error : validationErrors){
                errorMessage += "\n" + error;
            }
            Alert alert = new Alert(Alert.AlertType.ERROR, errorMessage);
            alert.show();
            return;
        }

        customer.setLastname(lastname);
        customer.setForename(forename);
        customer.setEmail(email);

        try {
            Alert alertConfirmation = new Alert(Alert.AlertType.CONFIRMATION, "Czy na pewno chcesz zapisać zmiany?");
            Optional<ButtonType> result = alertConfirmation.showAndWait();

            if (result.get() == ButtonType.OK){
                service.SaveCustomer(customer);
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Zmiany zostały zapisane");
                alert.show();
                this.fillView(customer.getId());
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.show();
            e.printStackTrace();
        }
    }

    public void showLoanDetails(MouseEvent mouseEvent) {
        TableView<Loan> table = (TableView<Loan>)mouseEvent.getSource();
        TableView.TableViewSelectionModel<Loan> row = table.getSelectionModel();
        if (mouseEvent.getClickCount() > 1 && !row.isEmpty()){
            try {
                Loan loan = row.getSelectedItem();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../../loans/views/loanDetails.fxml"));
                Parent root = loader.load();
                LoanDetailsController controller = loader.getController();
                controller.fillView(loan.getId(), -1, customer.getId());
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.setTitle("Szczegóły wypożyczenia książki");
                stage.setResizable(false);
                stage.show();
            } catch(Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
                alert.show();
                e.printStackTrace();
            }
        }
    }

    public void borrowBook(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../loans/views/loanDetails.fxml"));
            Parent root = loader.load();
            LoanDetailsController controller = loader.getController();
            controller.fillView(-1, -1, customer.getId());
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Szczegóły wypożyczenia książki");
            stage.setResizable(false);
            stage.show();
        } catch(Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.show();
            e.printStackTrace();
        }
    }
}
