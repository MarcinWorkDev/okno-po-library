package mwork.stud.po.library.modules.books.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import mwork.stud.po.library.models.Book;
import mwork.stud.po.library.models.Customer;
import mwork.stud.po.library.models.Loan;
import mwork.stud.po.library.modules.loans.controllers.LoanDetailsController;
import mwork.stud.po.library.services.ServiceLayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Main Controller in Book Module
 */
public class BookDetailsController {
    public TextField publisherText;
    public TextField isbnText;
    public TextField descriptionText;
    public TextField authorText;
    public TextField titleText;

    public TableView loansTable;
    public TableColumn issueDateColumn;
    public TableColumn returnDateColumn;
    public TableColumn customerColumn;

    public Text authorStaticText;
    public Text titleStaticText;
    public Button borrowBookButton;

    private ServiceLayer service;

    private Book book;
    private ObservableList<Loan> loansList = FXCollections.observableArrayList();

    public BookDetailsController(){
        this.service = new ServiceLayer();
    }

    /**
     * Fill the view on init
     * @param bookId Unique of Book
     */
    public void fillView(int bookId){

        if (bookId > 0) {
            this.book = service.GetBook(bookId);

            authorStaticText.setText(book.getAuthor());
            titleStaticText.setText(book.getTitle());

            authorText.setText(book.getAuthor());
            titleText.setText(book.getTitle());
            descriptionText.setText(book.getDescription());
            publisherText.setText(book.getPublisher());
            isbnText.setText(book.getIsbn());

            customerColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("customerName"));
            issueDateColumn.setCellValueFactory(new PropertyValueFactory<Loan, Date>("issueDate"));
            returnDateColumn.setCellValueFactory(new PropertyValueFactory<Loan, Date>("returnDate"));

            List<Loan> loans = book.getLoans();
            loansList = FXCollections.observableArrayList();
            for (Loan item : loans) {
                loansList.add(item);
            }
            loansTable.setItems(loansList);

        } else {
            book = new Book();
            authorStaticText.setText("Nie podano autora...");
            titleStaticText.setText("Nie podano tytułu...");
        }
    }

    public void saveChangesButton(ActionEvent actionEvent) {

        String author = authorText.getText();
        String title = titleText.getText();
        String description = descriptionText.getText();
        String publisher = publisherText.getText();
        String isbn = isbnText.getText();

        List<String> validationErrors = new ArrayList<>();
        if (author.trim().length() == 0){
            validationErrors.add("Nie podano autora");
        }
        if (title.trim().length() == 0){
            validationErrors.add("Nie podano tytułu");
        }

        if (validationErrors.size() > 0){
            String errorMessage = "Wprowadzone dane nie są prawidłowe:";
            for (String error : validationErrors){
                errorMessage += "\n" + error;
            }
            Alert alert = new Alert(Alert.AlertType.ERROR, errorMessage);
            alert.show();
            return;
        }

        book.setAuthor(author);
        book.setTitle(title);
        book.setDescription(description);
        book.setPublisher(publisher);
        book.setIsbn(isbn);

        try {
            Alert alertConfirmation = new Alert(Alert.AlertType.CONFIRMATION, "Czy na pewno chcesz zapisać zmiany?");
            Optional<ButtonType> result = alertConfirmation.showAndWait();

            if (result.get() == ButtonType.OK){
                service.SaveBook(book);
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Zmiany zostały zapisane");
                alert.show();
                this.fillView(book.getId());
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.show();
            e.printStackTrace();
        }
    }

    public void showLoanDetails(MouseEvent mouseEvent) {
        TableView<Loan> table = (TableView<Loan>)mouseEvent.getSource();
        TableView.TableViewSelectionModel<Loan> row = table.getSelectionModel();
        if (mouseEvent.getClickCount() > 1 && !row.isEmpty()){
            try {
                Loan loan = row.getSelectedItem();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../../loans/views/loanDetails.fxml"));
                Parent root = loader.load();
                LoanDetailsController controller = loader.getController();
                controller.fillView(loan.getId(), book.getId(), -1);
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.setTitle("Szczegóły wypożyczenia książki");
                stage.setResizable(false);
                stage.show();
            } catch(Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
                alert.show();
                e.printStackTrace();
            }
        }
    }

    public void borrowBook(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../../loans/views/loanDetails.fxml"));
            Parent root = loader.load();
            LoanDetailsController controller = loader.getController();
            controller.fillView(-1, book.getId(), -1);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Szczegóły wypożyczenia książki");
            stage.setResizable(false);
            stage.show();
        } catch(Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.show();
            e.printStackTrace();
        }
    }
}
