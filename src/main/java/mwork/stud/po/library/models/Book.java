package mwork.stud.po.library.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

/**
 * Book Entity
 */
@Entity
@Table(name = "BOOKS")
public class Book {
    /**
     * Id (Primary Key)
     */
    private int id;

    /**
     * Author or book
     */
    private String author;

    /**
     * Title of book
     */
    private String title;

    /**
     * Description of Book
     */
    private String description;

    /**
     * Publisher
     */
    private String publisher;

    /**
     * ISBN Number
     */
    private String isbn;

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public List<Loan> loans;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id")
    public List<Loan> getLoans(){
        return loans;
    }

    public void setLoans(List<Loan> loans){
        this.loans = loans;
    }
}
