package mwork.stud.po.library.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

/**
 * Customers of library table
 */
@Entity
@Table(name = "CUSTOMERS")
public class Customer {

    /**
     * Id (Primary Key)
     */
    private int id;

    /**
     * Forename
     */
    private String forename;

    /**
     * Last Name
     */
    private String lastname;

    /**
     * Email address
     */
    private String email;

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Loan> loans;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id")
    public List<Loan> getLoans(){
        return loans;
    }

    public void setLoans(List<Loan> loans){
        this.loans = loans;
    }
}
