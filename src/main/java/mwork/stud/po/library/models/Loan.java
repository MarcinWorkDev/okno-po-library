package mwork.stud.po.library.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Loans
 */
@Entity
@Table(name = "LOANS")
public class Loan {

    /**
     * Id (Primary Key)
     */
    private int id;

    /**
     * Book reference
     */
    private Book book;

    /**
     * Customer reference
     */
    private Customer customer;

    /**
     * Date when the book was borrowed
     */
    private Date issueDate;

    /**
     * Date when the book was returned
     */
    private Date returnDate;

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @ManyToOne
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @Transient
    public String getBookAuthor(){
        if (book != null){
            return book.getAuthor();
        }
        return "";
    }

    @Transient
    public String getBookTitle(){
        if (book != null){
            return book.getTitle();
        }
        return "";
    }

    @Transient
    public String getCustomerName(){
        if (customer != null){
            return customer.getLastname() + " " + customer.getForename();
        }
        return "";
    }
}
