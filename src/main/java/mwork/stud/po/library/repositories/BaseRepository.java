package mwork.stud.po.library.repositories;

import mwork.stud.po.library.services.utils.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Generic repository
 * @param <T> Any entity
 */
public abstract class BaseRepository<T> {

    private Class<T> typeClass;

    public BaseRepository(Class<T> typeClass){
        this.typeClass = typeClass;
    }

    /**
     * Get object by Id
     * @param id
     * @return object
     */
    public T Get(int id){
        T item;

        Session session = HibernateUtil.buildSessionFactory().openSession();
        try {
            item = session.get(typeClass, id);
        } finally {
            session.close();
        }

        return item;
    }

    /**
     * Get list of objects
     * @return list of objects
     */
    public List<T> Get(){
        List<T> items;

        Session session = HibernateUtil.buildSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(typeClass);
            items = criteria.list();
        } finally {
            session.close();
        }

        return items;
    }

    /**
     * Get list of objects using lazy loading for specified entities
     * @param lazyTables array of entities
     * @return list of objects
     */
    public List<T> Get(String[] lazyTables){
        List<T> items;

        Session session = HibernateUtil.buildSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(typeClass);
            for (String lazyTable : lazyTables){
                criteria.setFetchMode(lazyTable, FetchMode.LAZY);
            }
            items = criteria.list();
        } finally {
            session.close();
        }

        return items;
    }

    /**
     * Save object into database - add if new or update if already exists
     * @param item object
     * @throws Exception
     */
    public void Save(T item) throws Exception {
        Transaction trns = null;
        Session session = HibernateUtil.buildSessionFactory().openSession();
        try{
            trns = session.beginTransaction();
            session.saveOrUpdate(item);
            session.getTransaction().commit();
        } catch (Exception ex){
            if (trns != null){
                trns.rollback();
            }
            throw ex;
        } finally {
            session.close();
        }
    }

}
