package mwork.stud.po.library.repositories;

import mwork.stud.po.library.models.Book;
import mwork.stud.po.library.models.Loan;
import mwork.stud.po.library.services.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class LoanRepository extends BaseRepository<Loan> {

    public LoanRepository(){
        super(Loan.class);
    }

    /**
     * Check book availability
     * @param book Book to check
     * @return true if book is available, otherwise false
     */
    public boolean IsBookAvailable(Book book){
        List<Loan> items;
        Session session = HibernateUtil.buildSessionFactory().openSession();

        try{
            Query query = session.createQuery("FROM Loan x WHERE returnDate is null AND x.book = :book");
            query.setParameter("book", book);
            items = query.list();
            if (items.size() == 0){
                return true;
            }
        } catch (Exception ex){
            throw ex;
        } finally {
            session.close();
        }

        return false;
    }

}
