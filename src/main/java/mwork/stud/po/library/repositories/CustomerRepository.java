package mwork.stud.po.library.repositories;

import mwork.stud.po.library.models.Customer;

public class CustomerRepository extends BaseRepository<Customer> {

    public CustomerRepository(){
        super(Customer.class);
    }
}
