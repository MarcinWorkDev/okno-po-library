package mwork.stud.po.library.repositories;

import mwork.stud.po.library.models.Book;

public class BookRepository extends BaseRepository<Book> {

    public BookRepository() {
        super(Book.class);
    }

}
