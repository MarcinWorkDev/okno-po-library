package mwork.stud.po.library.services;

import mwork.stud.po.library.models.Book;
import mwork.stud.po.library.models.Customer;
import mwork.stud.po.library.models.Loan;
import mwork.stud.po.library.repositories.BookRepository;
import mwork.stud.po.library.repositories.CustomerRepository;
import mwork.stud.po.library.repositories.LoanRepository;

import java.util.Date;
import java.util.List;

/**
 * Primary Service Layer of Application
 */
public class ServiceLayer {

    private CustomerRepository customerRepository;
    private BookRepository bookRepository;
    private LoanRepository loanRepository;

    public ServiceLayer(){
        customerRepository = new CustomerRepository();
        bookRepository = new BookRepository();
        loanRepository = new LoanRepository();
    }

    /**
     * Get all customers
     * @return list of Customers
     */
    public List<Customer> GetCustomers(){

        return customerRepository.Get();
    }

    /**
     * Get all customers without loans
     * @return list of Customers
     */
    public List<Customer> GetCustomersWithoutLoans(){
        String[] lazyTables = new String[]{ "loans" };

        return customerRepository.Get(lazyTables);
    }

    /**
     * Get customer object
     * @param customerId Unique of Customer
     * @return Customer
     */
    public Customer GetCustomer(int customerId){

        return customerRepository.Get(customerId);
    }

    /**
     * Get all books
     * @return list of Books
     */
    public List<Book> GetBooks(){

        return bookRepository.Get();
    }

    /**
     * Get all books without loans
     * @return list of Books
     */
    public List<Book> GetBooksWithoutLoans(){
        String[] lazyTables = new String[]{ "loans" };

        return bookRepository.Get(lazyTables);
    }

    /**
     * Get book
     * @param bookId Unique of Book
     * @return Book
     */
    public Book GetBook(int bookId){

        return bookRepository.Get(bookId);
    }

    /**
     * Get loan
     * @param loanId Unique of Loan
     * @return Loan
     */
    public Loan GetLoan(int loanId) {

        return loanRepository.Get(loanId);
    }

    /**
     * Add new customer into database or update existing one
     * @param customer Customer
     * @throws Exception
     */
    public void SaveCustomer(Customer customer) throws Exception {

        customerRepository.Save(customer);
    }

    /**
     * Add new book into database or update existing one
     * @param book
     * @throws Exception
     */
    public void SaveBook(Book book) throws Exception {

        bookRepository.Save(book);
    }

    /**
     * Borrow book
     * @param loan Loan
     * @throws Exception If book is already borrowed
     */
    public void BorrowBook(Loan loan) throws Exception {
        if (loanRepository.IsBookAvailable(loan.getBook())){
            loan.setIssueDate(new Date());
            loanRepository.Save(loan);
        } else {
            throw new Exception("Książka jest aktualnie wypożyczona");
        }
    }

    /**
     * Return book
     * @param loan Loan
     * @throws Exception
     */
    public void ReturnBook(Loan loan) throws Exception {
        loan.setReturnDate(new Date());
        loanRepository.Save(loan);
    }
}
