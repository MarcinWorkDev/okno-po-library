package mwork.stud.po.library.services.utils;

import org.hibernate.cfg.Configuration;
import org.hibernate.SessionFactory;

public class HibernateUtil {

    public static SessionFactory buildSessionFactory() {
        try {

            return new Configuration().configure("/hibernate.cfg.xml").buildSessionFactory();

        } catch (Throwable ex) {

            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}
